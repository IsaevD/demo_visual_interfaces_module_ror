$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "visual_interfaces_d/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "visual_interfaces_d"
  s.version     = VisualInterfacesD::VERSION
  s.authors     = "Denis Isaev"
  s.email       = "isaevdenismich@mail.ru"
  s.homepage    = "www.den-isaev.com"
  s.summary     = "Базовый модуль предоставления графических интерфейсов системы"
  s.description = "Базовый модуль 0-ого уровня для предоставления графических интерфейсов системы"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1.0"

  s.add_development_dependency "sqlite3"
end
