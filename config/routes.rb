VisualInterfacesD::Engine.routes.draw do

  match "/example/static_popup",  to: "example#static_popup", via: "get"
  match "/example/card",  to: "example#card", via: "get"
  match "/example/table",  to: "example#table", via: "get"

end
