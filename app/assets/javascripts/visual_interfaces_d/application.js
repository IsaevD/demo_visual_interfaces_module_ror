// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require tinymce-jquery
//= require_tree .

$(function(){
    initializeMenu();
    initializeFileField();
});

// Инициализация бокового меню
function initializeMenu() {
    var has_second_menu_class= ".has_second_menu";
    var second_menu_class = ".second_menu";
    $(has_second_menu_class).each(function(index, element){
        $(element).next(second_menu_class).hide();
    });
    $("body").bind("click", has_second_menu_class, function(eventObject){
        var submenu = $(eventObject.target).next(second_menu_class);
        changeSecondMenuVisibleState($(eventObject.target), submenu);
    });

    function changeSecondMenuVisibleState(first_menu, second_menu) {
        var icon_arrow_down_class = ".icon_arrow_down";
        var icon_arrow_up_class = ".icon_arrow_up";
        if (second_menu.is(":visible")) {
            second_menu.hide();
            subClasses(first_menu.children(icon_arrow_up_class), icon_arrow_up_class, icon_arrow_down_class);
        }
        else {
            second_menu.show();
            subClasses(first_menu.children(icon_arrow_down_class), icon_arrow_down_class, icon_arrow_up_class);
        }
    }
}

// Инициализация удаления фотографии
function initializeFileField() {
    var delete_file_icon_class = ".file_field .icon-delete";
    var current_file_class = ".current_file";
    var file_hidden_value_class = ".file_field input[type=hidden]";
    var delete_flag = "nil";
    $("body").bind("click", delete_file_icon_class, function(eventObject){
        parent = $(eventObject.target).parent().parent()
        parent.children(current_file_class).remove();
        parent.children(file_hidden_value_class).val(delete_flag);
    });
}

// Общие функции
function subClasses(object, removed_class, added_class) {
    $(object)
        .removeClass(removed_class.replace(".", ""))
        .addClass(added_class.replace(".", ""))
}
